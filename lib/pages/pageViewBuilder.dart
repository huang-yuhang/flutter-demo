/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2024-01-20 11:46:51
 */
import 'package:flutter/material.dart';

class PageViewBuilderPage extends StatefulWidget {
  const PageViewBuilderPage({super.key});

  @override
  State<PageViewBuilderPage> createState() => _PageViewBuilderPageState();
}

class _PageViewBuilderPageState extends State<PageViewBuilderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('page_view')),
      body: PageView.builder(
          itemCount: 10,
          itemBuilder: (contex, index) {
            return Center(
                child: Text('第${index + 1}屏',
                    style: Theme.of(context).textTheme.headlineLarge));
          }),
    );
  }
}
