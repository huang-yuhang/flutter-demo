import 'package:flutter/material.dart';
import 'package:flutter01/pages/form.dart';
import 'package:flutter01/pages/news.dart';
import 'package:flutter01/pages/search.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({super.key});

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return const SearchPage();
                }));
              },
              child: const Text('搜索')),
          const SizedBox(height: 20),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/form',
                    arguments: {"name": "hang"});
              },
              child: const Text('表单')),
          const SizedBox(height: 20),
          ElevatedButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return const NewsPage(title: '我是标题');
                }));
              },
              child: const Text('新闻1')),
          const SizedBox(height: 20),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/news');
              },
              child: const Text('新闻2')),
        ],
      ),
    );
  }
}
