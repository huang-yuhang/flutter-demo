import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: 4,
      vsync: this,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: const Size.fromHeight(50),
          child: AppBar(
              backgroundColor: Colors.white,
              title: SizedBox(
                  height: 30,
                  child: TabBar(
                      isScrollable: true,
                      controller: _tabController,
                      indicatorColor: Colors.green.shade400,
                      // indicatorPadding: const EdgeInsets.all(5),
                      indicatorSize: TabBarIndicatorSize.label,
                      labelColor: Colors.green.shade400,
                      unselectedLabelColor: Colors.black,
                      tabs: const [
                        Tab(child: Text('关注1')),
                        Tab(child: Text('关注2')),
                        Tab(child: Text('关注3')),
                        Tab(child: Text('关注4')),
                      ])))),
      body: TabBarView(controller: _tabController, children: [
        ListView(children: const [ListTile(title: Text('我是关注1'))]),
        ListView(children: const [ListTile(title: Text('我是关注2'))]),
        ListView(children: const [ListTile(title: Text('我是关注3'))]),
        ListView(children: const [ListTile(title: Text('我是关注4'))]),
      ]),
    );
  }
}
