import 'package:flutter01/widgets/myDialog.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';

class DialogPage extends StatefulWidget {
  const DialogPage({super.key});

  @override
  State<DialogPage> createState() => _DialogPageState();
}

class _DialogPageState extends State<DialogPage> {
  alertDialog() async {
    var res = await showDialog(
        context: context,
        builder: (builder) {
          return AlertDialog(
            title: const Text('提示'),
            content: const Text('确认删除?'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop(1);
                  },
                  child: const Text('确定')),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop(2);
                  },
                  child: const Text('取消')),
            ],
          );
        });

    print(res);
  }

  simpleDialog() async {
    var res = await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (builder) {
          return SimpleDialog(
            title: const Text('请选择'),
            children: [
              SimpleDialogOption(
                  onPressed: () => Navigator.of(context).pop(1),
                  child: const Text('1')),
              SimpleDialogOption(
                  onPressed: () => Navigator.of(context).pop(2),
                  child: const Text('2')),
              SimpleDialogOption(
                  onPressed: () => Navigator.of(context).pop(3),
                  child: const Text('3')),
            ],
          );
        });
    print(res);
  }

  _showModalBottomSheet() async {
    var res = await showModalBottomSheet(
        context: context,
        builder: (builder) {
          return SizedBox(
            height: 200,
            child: Column(children: [
              ListTile(
                title: const Text('_showModalBottomSheet'),
                onTap: () => Navigator.of(context).pop('onTap'),
                onLongPress: () => Navigator.of(context).pop('onLongPress'),
              ),
              ListTile(
                title: const Text('_showModalBottomSheet'),
                onTap: () => Navigator.of(context).pop('onTap'),
                onLongPress: () => Navigator.of(context).pop('onLongPress'),
              ),
              ListTile(
                title: const Text('_showModalBottomSheet'),
                onTap: () => Navigator.of(context).pop('onTap'),
                onLongPress: () => Navigator.of(context).pop('onLongPress'),
              ),
              ListTile(
                title: const Text('_showModalBottomSheet'),
                onTap: () => Navigator.of(context).pop('onTap'),
                onLongPress: () => Navigator.of(context).pop('onLongPress'),
              ),
            ]),
          );
        });

    print(res);
  }

  _fluttertoast() async {
    Fluttertoast.showToast(
      msg: 'msg',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
    );
  }

  _customDialog() async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (builder) {
          return MyDialog(
            title: 'title',
            content: 'content',
            onTap: () {
              Navigator.of(context).pop();
            },
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
              onPressed: () => alertDialog(), child: const Text('alertDialog')),
          const SizedBox(height: 20),
          ElevatedButton(
              onPressed: simpleDialog, child: const Text('simpleDialog')),
          const SizedBox(height: 20),
          ElevatedButton(
              onPressed: _showModalBottomSheet,
              child: const Text('showModalBottomSheet')),
          const SizedBox(height: 20),
          ElevatedButton(
              onPressed: _fluttertoast, child: const Text('fluttertoast')),
          const SizedBox(height: 20),
          ElevatedButton(
              onPressed: _customDialog, child: const Text('自定义dialog')),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
