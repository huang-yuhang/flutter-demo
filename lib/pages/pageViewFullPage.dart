/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2024-01-20 11:56:20
 */
import 'package:flutter/material.dart';

class PageViewFullPage extends StatefulWidget {
  const PageViewFullPage({super.key});

  @override
  State<PageViewFullPage> createState() => _PageViewFullPageState();
}

class _PageViewFullPageState extends State<PageViewFullPage> {
  List<Widget> list = [];

  @override
  void initState() {
    super.initState();
    for (var i = 0; i < 5; i++) {
      list.add(Center(
          child: Text('${i + 1}---', style: const TextStyle(fontSize: 20))));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('page_view')),
      body: PageView(
        scrollDirection: Axis.vertical,
        children: list,
        onPageChanged: (index) {
          if (list.length - index == 2) {
            setState(() {
              for (var i = 0; i < 5; i++) {
                list.add(Center(
                    child: Text('add ${i + 1}---',
                        style: const TextStyle(fontSize: 20))));
              }
            });
          }
        },
      ),
    );
  }
}
