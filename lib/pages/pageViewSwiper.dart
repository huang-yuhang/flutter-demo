import 'package:flutter/material.dart';
import '../widgets/swiper.dart';

class PageViewSwiperPage extends StatefulWidget {
  final double width;
  final double height;
  const PageViewSwiperPage({
    super.key,
    this.width = double.infinity,
    this.height = 200,
  });

  @override
  State<PageViewSwiperPage> createState() => _PageViewSwiperPageState();
}

class _PageViewSwiperPageState extends State<PageViewSwiperPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('swiper')),
      body: ListView(children: const [
        Swiper(list: [
          'assets/images/01.jpg',
          'assets/images/02.jpg',
          'assets/images/03.jpg',
        ]),
      ]),
    );
  }
}
