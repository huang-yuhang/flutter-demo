/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2024-01-20 11:41:47
 */
import 'package:flutter/material.dart';

class PageViewPage extends StatefulWidget {
  const PageViewPage({super.key});

  @override
  State<PageViewPage> createState() => _PageViewPageState();
}

class _PageViewPageState extends State<PageViewPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('page_view')),
      body: PageView(scrollDirection: Axis.vertical, children: [
        Center(
            child: Text('1', style: Theme.of(context).textTheme.headlineLarge)),
        Center(
            child: Text('2', style: Theme.of(context).textTheme.headlineLarge)),
        Center(
            child: Text('3', style: Theme.of(context).textTheme.headlineLarge)),
        Center(
            child: Text('4', style: Theme.of(context).textTheme.headlineLarge)),
        Center(
            child: Text('5', style: Theme.of(context).textTheme.headlineLarge)),
        Center(
            child: Text('6', style: Theme.of(context).textTheme.headlineLarge)),
      ]),
    );
  }
}
