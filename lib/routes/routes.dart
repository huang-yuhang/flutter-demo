import 'package:flutter/material.dart';
import 'package:flutter01/pages/dialog.dart';
import 'package:flutter01/pages/form.dart';
import 'package:flutter01/pages/keepAlive.dart';
import 'package:flutter01/pages/news.dart';
import 'package:flutter01/pages/pageViewBuilder.dart';
import 'package:flutter01/pages/pageViewFullPage.dart';
import 'package:flutter01/pages/pageViewSwiper.dart';
import 'package:flutter01/pages/search.dart';
import 'package:flutter01/pages/user/login.dart';
import 'package:flutter01/pages/user/register/step1.dart';
import 'package:flutter01/pages/user/register/step2.dart';
import 'package:flutter01/pages/user/register/step3.dart';
import 'package:flutter01/pages/pageView.dart';
import '../main.dart';

// 配置路由
Map routes = {
  '/': (ctx) => const HomePage(),
  '/news': (ctx) => const NewsPage(),
  '/form': (ctx, {arguments}) => FormPage(arguments: arguments),
  '/search': (ctx) => const SearchPage(),
  '/login': (ctx) => const LoginPage(),
  '/registerStep1': (ctx) => const RegisterStep1Page(),
  '/registerStep2': (ctx) => const RegisterStep2Page(),
  '/registerStep3': (ctx) => const RegisterStep3Page(),
  '/dialog': (ctx) => const DialogPage(),
  '/pageView': (ctx) => const PageViewPage(),
  '/pageViewBuilder': (ctx) => const PageViewBuilderPage(),
  '/pageViewFullPage': (ctx) => const PageViewFullPage(),
  '/pageViewSwiperPage': (ctx) => const PageViewSwiperPage(),
  '/keepAlivePage': (ctx) => const KeepAlivePage(),
};

// 创建路由触发
var onGenerateRoute = (RouteSettings settings) {
  final String? name = settings.name;
  final Function? pageContentBuilder = routes[name];
  if (pageContentBuilder != null) {
    if (settings.arguments != null) {
      final Route route = MaterialPageRoute(
          builder: (context) =>
              pageContentBuilder(context, arguments: settings.arguments));
      return route;
    } else {
      final Route route =
          MaterialPageRoute(builder: (context) => pageContentBuilder(context));
      return route;
    }
  }
  return null;
};
