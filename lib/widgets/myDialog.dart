import 'package:flutter/material.dart';

class MyDialog extends StatefulWidget {
  final String title;
  final String content;
  final Function()? onTap;
  const MyDialog(
      {super.key, required this.title, required this.content, this.onTap});

  @override
  State<MyDialog> createState() => _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Center(
          child: Container(
        width: 200,
        height: 200,
        color: Colors.white,
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.all(5),
            child: Stack(
              children: [
                Align(
                    alignment: Alignment.topCenter, child: Text(widget.title)),
                Align(
                    alignment: Alignment.centerRight,
                    child: InkWell(
                      onTap: widget.onTap,
                      child: const Icon(Icons.close),
                    )),
              ],
            ),
          ),
          const Divider(),
          Container(
            padding: const EdgeInsets.all(10),
            width: double.infinity,
            child: Text(widget.content),
          )
        ]),
      )),
    );
  }
}
